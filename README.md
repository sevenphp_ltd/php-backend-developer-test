# PHP Backend Developer Test

> Please note that this is a fictional requirement. 
> The scenario is meant to serve as a basic technical test to help us gauge your level of experience with PHP & approach towards coding a web-based system.
> If you cannot complete the task then please present the code you wrote and then an explanation of how you would have completed any remaining tasks.


## Scenario For This Test

Make a simple 'To-Do' web app with the following :

- Add an item (a to-do item)
- Edit an existing item
- Mark as completed
- Delete items (all at once and selectively)
- Searching the to-do list by title


## Other Requirements:

- This web app should be protected behind a login page
- Two user modes: admin (full access) and normal user (read-only access)
- Adhere to PSR-2 coding standards (http://www.php-fig.org/psr/psr-2/)

## Constraints -  Dont's

- You cannot use full-stack PHP frameworks (e.g: Zend Framework, Symfony..etc)
- You cannot use micro frameworks (e.g: silex)
- You cannot use CMSes (e.g: drupal, wordpress)


## Constraints -  Do's

- You can re-use components or opensource libraries to fit your needs and to even create your own small framework (e.g Symfony Components)
- You can re-use html/css templates available online, including an "HTML/CSS/JS" framework like Twitter Bootstrap


## Want to really impress us?

- Provide a technical documentation
- Use composer
- Unit test your code
- Make your code available in a private BitBucket repo

Good luck!

##### Disclaimer
Candidates are entirely responsible for their own time and costs associated in completing the technical test, and SevenPHP Ltd is under no obligation to progress beyond this stage.
